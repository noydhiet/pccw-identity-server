# PCCW INDENDITY SERVER


[![pipeline status](https://gitlab.mihpccw.com/core-services/pccw-identity-server/badges/development/pipeline.svg)](https://gitlab.mihpccw.com/core-services/pccw-identity-server/commits/development)

## Installation

This application runs with Node JS and use MongoDB.
Please set up MongoDB before the instalation.

Follow the bellow Commonds 

```bash
npm install

npm start
```

## Configurations 

Change the configurations file: nodemon.json

Create a strong secret key and save it to JWT_KEY.

Frontend appication Domain name goes under FRONT_END_URL.

```

{
    "env": {
        "MONGO_DB": "mongodb://localhost:27017/myIndiHomeUsers",
        "JWT_KEY": "secret",
        "FRONT_END_URL": "myindihome"
    }
}
```

## License
PCCW SOLUTIONS