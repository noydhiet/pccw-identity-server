const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        req.userData = decoded;
        next();
    } catch (error) {
       
    if (error.name == 'TokenExpiredError') {
        return res.status(200).json({
            ok:false,
            status:402,
            message: 'jwt expired',
            data:{}
        });
    }else{
        return res.status(200).json({
            ok:false,
            status:401,
            message: 'Auth failed',
            data:{}
        });
    }
    }
};