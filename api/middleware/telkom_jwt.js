'use strict';

const util = require("util");
const request = require("request-promise");
//const statusCode = require('../http-status/status_code');
//const log4js = require('log4js');
// const logger = log4js.getLogger('ClinetAuthenticationService');

module.exports.getJWTFForUser = function () {

  return new Promise(function (resolve, reject) {

    try {
      // fixme: config
      let token = getJWTF("delta", "deltauser123", "9610f3c5-1ea7-4ad1-8b82-4f2dbc5a7930");
      console.log("result comes here ",token);
      resolve(token);

    } catch (error) {
     // logger.error(error);
      reject();
    }
  });
}

/**
 * Get access token form the cline API useing Basic Authentication
 *
 * user String username
 * secret String password 
 **/
async function getJWT(user, secret, app_id) {
    let options = {
      method: 'GET',
      // fixme: config
      url: 'https://apigwdev.telkom.co.id:7777/rest/pub/apigateway/jwt/getJsonWebToken',
      qs: { app_id: app_id },
      headers:
      {
        Authorization: 'Basic ' + Buffer.from(user + ':' + secret).toString('base64'),
        Accept: 'application/json',
        "Content-Type": "application/json"
      },
      json: true,
      strictSSL: false
    };
  
    let resp = await request(options);
    if (!util.isNullOrUndefined(resp)) {
        console.log(resp.jwt);
      return resp.jwt;
    }
    else {
      return null;
    }
  
  }
  

