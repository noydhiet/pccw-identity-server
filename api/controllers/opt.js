const OtpManager = require("../optService/OtpManager");
const otpRepository = require("../optService/otpRepository");
const otpSender = require("../optService/otpSender")
const otpManager = new OtpManager(otpRepository, { otpLength: 4, validityTime: 3 });
const jwt = require("jsonwebtoken");
const User = require("../models/user");
const Mobile = require("../models/mobile");
const mongoose = require("mongoose");
const mobileService = require("../services/mobileService");

//Logging
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'info';


exports.create_otp = (req, res) => {
    otpManager.create(req.params.token, req, creatingOTP, res);
}


function creatingOTP(otp, req, res) {
    if (otp == 402) {
        statusCode = 200;
        bodyMessage = "User temporarily blocked to send OTP.";
        res.status(statusCode).json({
            ok: false,
            status: 402,
            message: bodyMessage,
            data: {}
        });
    }
    if (otp == 500) {
        res.status(200).json({
            ok: false,
            status: 406,
            message: "Service error",
            data: {}
        });
    }

    if(otp.code){
        otpSender.send(otp, req.body, res);
    } 
}




function verifyOTPResult(req,res,verificationResults,verificationResult) {

    const verificationType = req.params.type;
    let statusCode;
    let bodyMessage;
    var tokenIn;

    let remainingAttempts = 2;

    mobileService.noOFAttempts(req).then(
        fulfilled => {
            console.log("the attempts", fulfilled);
            if (fulfilled < 0) {
                remainingAttempts = 0;
            } else {
                remainingAttempts = fulfilled;
            }
        }
    ).catch(error => {
        remainingAttempts = 0;
        logger.error('Error.');
        logger.error(error);
    }).finally(function () {
        switch (verificationResult) {
            case verificationResults.valid:
                if (verificationType === 'direct') {

                    let mobileNumber = req.body.mobile;
                    if(mobileNumber.substring(0, 2) === '62'){
                        mobileNumber = '0'+ mobileNumber.substring(2);
                    } 

                    User.find({ mobile: mobileNumber })
                        .exec()
                        .then(user => {
                            if (!user[0]) {
                                statusCode = 200;
                                bodyMessage = "No account with this mobile number.";
                                res.status(statusCode).json({
                                    ok: false,
                                    status: 404,
                                    message: bodyMessage,
                                    data: {}
                                });

                            }
                            else if (user[0].status === 'locked') {
                                return res.status(200).json({
                                    ok: false,
                                    status: 402,
                                    message: "User profile is locked. Please activate it",
                                    data: { userActiveToken: user[0].userActiveToken }

                                });
                            }
                            else {

                                const token = jwt.sign(
                                    {
                                        email: user[0].email,
                                        userId: user[0]._id,
                                        userRole:user[0].userRole,
                                        mobile:user[0].mobile
                                    },
                                    process.env.JWT_KEY,
                                    {
                                        expiresIn: process.env.JWT_KEY_LIFE
                                    }
                                );

                                const refreshToken = jwt.sign(
                                    {
                                        email: user[0].email,
                                        userId: user[0]._id,
                                        userRole:user[0].userRole,
                                        mobile:user[0].mobile
                                    },
                                    process.env.JWT_KEY,
                                    {
                                        expiresIn: process.env.JWT_REFRESH_KEY_LIFE
                                    }
                                );
            

                                statusCode = 200;
                                bodyMessage = "OK";
                                res.status(statusCode).json({
                                    ok: true,
                                    status: 200,
                                    message: bodyMessage,
                                    data: { 
                                        token: token,
                                        tokenExpiresIn:process.env.JWT_KEY_LIFE,
                                        refreshToken: refreshToken,
                                        refreshTokenExpiresIn:process.env.JWT_REFRESH_KEY_LIFE,
                                        userName: user[0].name,
                                        userId: user[0]._id,
                                        type:"generalUser"}
                                });


                            }
                        })
                        .catch(err => {
                            console.log(err);
                            statusCode = 500;
                            bodyMessage = "Error";
                            res.status(statusCode).json({
                                ok: false,
                                status: 500,
                                message: bodyMessage,
                                data: {}
                            });
                        });

                    break;

                } else {
                    statusCode = 200;
                    bodyMessage = "OK";
                    res.status(statusCode).json({
                        ok: true,
                        status: 200,
                        message: bodyMessage,
                        data: {}
                    });
                    break;
                }

            case verificationResults.notValid:
                statusCode = 200;
                bodyMessage = "Not found"
                res.status(statusCode).json({
                    ok: false,
                    status: 404,
                    message: bodyMessage,
                    data: {
                        validityTime: 3,
                        noOfAttempts: remainingAttempts-1
                    }
                });
                break;
            case verificationResults.checked:
                statusCode = 200;
                bodyMessage = "The code has already been verified";
                res.status(statusCode).json({
                    ok: false,
                    status: 409,
                    message: bodyMessage,
                    data: {
                        validityTime: 3,
                        noOfAttempts: remainingAttempts-1
                    }
                });
                break;
            case verificationResults.expired:
                statusCode = 200;
                bodyMessage = "The code is expired";
                res.status(statusCode).json({
                    ok: false,
                    status: 401,
                    message: bodyMessage,
                    data: {
                        validityTime: 3,
                        noOfAttempts: remainingAttempts-1
                    }
                });
                break;

            case verificationResults.blocked:
                statusCode = 200;
                bodyMessage = "User Blocked";
                res.status(statusCode).json({
                    ok: false,
                    status: 402,
                    message: bodyMessage,
                    data: {}
                });
                break;

            default:
                statusCode = 200;
                bodyMessage = "The code is invalid for unknown reason";
                res.status(statusCode).json({
                    ok: false,
                    status: 403,
                    message: bodyMessage,
                    data: {
                        validityTime: 3,
                        noOfAttempts: remainingAttempts-1
                    }
                });
        }
    });
   
}

exports.verify_otp = (req, res) => {
    otpManager.verify(req.params.token, req.params.code, req, res,verifyOTPResult);
}
