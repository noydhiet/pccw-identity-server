const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/user");
var async = require('async');
var crypto = require('crypto');
const guestService = require("../services/guestService");
const notificationService = require("../services/notificationService");
const joi = require('@hapi/joi');
const {isBlacklistedEmail} = require("../services/userValidationService");

//Logging
var log4js = require('log4js');
var logger = log4js.getLogger('User Controller');
logger.level = 'info';


/**
 * This method is used to check Email and Mobile
 */
exports.user_check = (req, res, next) => {

    logger.info(`checking for the user as ${JSON.stringify(req.query)}`);

    if (req.query.type === 'email') {
        let emailAdress = req.query.value.toLowerCase();

        // validate the email with the blacklisted emails
        if (isBlacklistedEmail(emailAdress)) {
            return res.status(200).json({
                ok: true,
                status: 403,
                message: "blacklisted email address",
                data: {}
            });
        }
        User.find({ email: emailAdress })
            .exec()
            .then(user => {

            if (user.length >= 1) {

              if (user[0].status === 'migrated') {
                                   return res.status(200).json({
                                          ok: false,
                                          status: 402,
                                          message: "User profile is incomplete. Please complete",
                                          data: { resetPasswordToken: user[0].resetPasswordToken,
                                          resetPasswordExpires:user[0].resetPasswordExpires }
                                                             });
                                                         }


                return res.status(200).json({
                    ok: true,
                    status: 200,
                    message: "Email registered",
                    data: {
                     isUser:true,
                     email: user[0].email,
                     mobile:user[0].mobile
                    }
                });

            }else{
                return res.status(200).json({
                    ok: false,
                    status: 404,
                    message: "Email not registered",
                    data: {
                     isUser:false
                    }
                });

                }

            }).catch(err => {

                logger.error('Error.');
                logger.error(err);

                res.status(500).json({
                    ok: false,
                    status: 500,
                    message: err,
                    data: {}
                });
            });

    }
    if (req.query.type === 'mobile') {

        let mobileNumber = req.query.value;
        if (mobileNumber.substring(0, 2) === '62') {
            mobileNumber = '0' + mobileNumber.substring(2);
        }

        User.find({ mobile:mobileNumber })
        .exec()
        .then(user => {
            if (user.length >= 1) {

                 if (user[0].status === 'migrated') {
                      return res.status(200).json({
                             ok: false,
                             status: 402,
                             message: "User profile is incomplete. Please complete",
                             data: { resetPasswordToken: user[0].resetPasswordToken,
                             resetPasswordExpires:user[0].resetPasswordExpires }
                                                });
                                            }

                return res.status(200).json({
                    ok: true,
                    status: 200,
                    message: "Mobile already registered",
                    data: {
                     isUser:true,
                     email: user[0].email,
                     mobile:user[0].mobile
                    }
                });

            }else{

                    return res.status(200).json({
                        ok: false,
                        status: 404,
                        message: "Mobile not registered",
                        data: {
                            isUser: false
                        }
                    });

                }

            }).catch(err => {

                logger.error('Error.');
                logger.error(err);

                res.status(500).json({
                    ok: false,
                    status: 500,
                    message: err,
                    data: {}
                });
            });
    }

}

/**
 * This method is used to sign up a user
 */
exports.user_signup = (req, res, next) => {

    let emailIn = req.body.email.toLowerCase();
    User.find({ email: emailIn })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                if (user[0].status === 'locked') {
                    return res.status(200).json({
                        ok: false,
                        status: 402,
                        message: "User profile is locked. Please activate it",
                        data: { userActiveToken: user[0].userActiveToken }

                    });
                }
                return res.status(200).json({
                    ok: false,
                    status: 409,
                    message: "Email already registered",
                    data: {}
                });
            }
            else {

                let mobileNumber = req.body.mobile;

                if (mobileNumber.substring(0, 2) === '62') {
                    mobileNumber = '0' + mobileNumber.substring(2);
                }

                User.find({ mobile: mobileNumber })
                    .exec()
                    .then(user => {
                        if (user.length >= 1) {

                            logger.info('Mobile Number already registered.');
                            logger.info(req.body.mobile);


                            if (user[0].status === 'locked') {
                                return res.status(200).json({
                                    ok: false,
                                    status: 402,
                                    message: "User profile is locked. Please activate it",
                                    data: { userActiveToken: user[0].userActiveToken }

                                });
                            }


                            return res.status(200).json({
                                ok: false,
                                status: 410,
                                message: "Mobile Number already registered",
                                data: {}

                            });
                        } else {
                            bcrypt.hash(req.body.password, 10, (err, hash) => {
                                if (err) {
                                    logger.info('Error.');
                                    logger.info(err);

                                    return res.status(500).json({
                                        ok: false,
                                        status: 500,
                                        message: err,
                                        data: {}
                                    });
                                } else {
                                    var tokenUserActive = crypto.randomBytes(20).toString('hex');

                                    let emailAdress = req.body.email.toLowerCase();
                                    const user = new User({
                                        _id: new mongoose.Types.ObjectId(),
                                        email: emailAdress,
                                        name: req.body.name,
                                        password: hash,
                                        mobile: mobileNumber,
                                        userRole: [req.body.userRole],
                                        indiHomeNumber: req.body.indiHomeNumber,
                                        refCode: req.body.refCode,
                                        status: 'locked',
                                        userActiveToken: tokenUserActive,
                                        resetPasswordToken: null,
                                        resetPasswordExpires: null
                                    });
                                    user
                                        .save()
                                        .then(result => {
                                            logger.info('User created.');
                                            logger.info(JSON.stringify(result));
                                            res.status(201).json({
                                                ok: true,
                                                status: 201,
                                                message: "User created",
                                                data: { tokenUserActive: tokenUserActive }
                                            });
                                        })
                                        .catch(err => {
                                            logger.error('Error.');
                                            logger.error(err);
                                            res.status(500).json({
                                                ok: false,
                                                status: 500,
                                                message: err,
                                                data: {}
                                            });
                                        });
                                }
                            });
                        }
                    });

            }
        })
        .catch(err => {

            logger.error('Error.');
            logger.error(err);

            res.status(500).json({
                ok: false,
                status: 500,
                message: err,
                data: {}
            });
        });

};


/**
 * This method is used to login with username of password
 */

exports.user_login = (req, res) => {

    let emailIn = req.body.email.toLowerCase();
    // validate the email with the blacklisted emails
    if (isBlacklistedEmail(emailIn)) {
        return res.status(200).json({
            ok: true,
            status: 403,
            message: "blacklisted email address",
            data: {}
        });
    }

    User.find({ email: emailIn })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(200).json({
                    ok: false,
                    status: 404,
                    message: "Email not found",
                    data: {}
                });
            }
            if (user[0].status === 'locked') {
                return res.status(200).json({
                    ok: false,
                    status: 402,
                    message: "User profile is locked. Please activate it",
                    data: { userActiveToken: user[0].userActiveToken }

                });
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(200).json({
                        ok: false,
                        status: 401,
                        message: "Wrong login credentials",
                        data: {}

                    });
                }
                if (result) {
                    const token = jwt.sign(
                        {
                            email: user[0].email,
                            userId: user[0]._id,
                            userRole: user[0].userRole,
                            mobile: user[0].mobile
                        },
                        process.env.JWT_KEY,
                        {
                            expiresIn: process.env.JWT_KEY_LIFE
                        }
                    );

                    const refreshToken = jwt.sign(
                        {
                            email: user[0].email,
                            userId: user[0]._id,
                            userRole: user[0].userRole,
                            mobile: user[0].mobile
                        },
                        process.env.JWT_REFRESH_KEY,
                        {
                            expiresIn: process.env.JWT_REFRESH_KEY_LIFE
                        }
                    );

                    return res.status(200).json({
                        ok: true,
                        message: "login successful",
                        status: 200,
                        data: {
                            token: token,
                            tokenExpiresIn: process.env.JWT_KEY_LIFE,
                            refreshToken: refreshToken,
                            refreshTokenExpiresIn: process.env.JWT_REFRESH_KEY_LIFE,
                            userName: user[0].name,
                            userId: user[0]._id,
                            type: "generalUser"
                        }
                    });
                }
                res.status(200).json({
                    ok: false,
                    status: 401,
                    message: "Wrong login credentials",
                    data: {}
                });
            });
        })
        .catch(err => {
            logger.error('Error.');
            logger.error(err);
            res.status(500).json({
                ok: false,
                message: err,
                data: {}
            });
        });
};



/**
 * To get a token with the refresh token
 */

exports.refresh_token = (req, res) => {
    const postData = req.body;
    try {

        if (postData.refreshToken) {
            const decoded = jwt.verify(postData.refreshToken, process.env.JWT_REFRESH_KEY);
            let email = decoded.email;
            let userId = decoded.userId;
            let userRole = decoded.userRole;
            let mobile = decoded.mobile;

            const token = jwt.sign(
                {
                    email: email,
                    userId: userId,
                    userRole: userRole,
                    mobile: mobile
                },
                process.env.JWT_KEY,
                {
                    expiresIn: process.env.JWT_KEY_LIFE
                }
            );

            return res.status(200).json({
                ok: true,
                message: "refresh successful",
                status: 200,
                data: {
                    token: token,
                    tokenExpiresIn: process.env.JWT_KEY_LIFE,
                    type: "generalUser"
                }
            });

        } else {

            return res.status(200).json({
                ok: false,
                message: "Auth failed",
                status: 401,
                data: {
                }
            });

        }


    } catch (error) {

        if (error.name == 'TokenExpiredError') {
            return res.status(200).json({
                ok: false,
                status: 402,
                message: 'jwt expired',
                data: {}
            });
        } else {
            return res.status(200).json({
                ok: false,
                status: 401,
                message: 'Auth failed',
                data: {}
            });
        }
    }

}

/**
 * The guest users login
 */
exports.guest_login = (req, res) => {


    guestService.saveNewGuest(req).then(
        fulfilled => {
            const token = jwt.sign(
                {
                    creationDate: fulfilled.creationDate,
                    id: fulfilled._id,
                    userRole: ["guest"]
                },
                process.env.JWT_KEY,
                {
                    expiresIn: "1h"
                }
            );
            return res.status(200).json({
                ok: true,
                message: "login successful",
                status: 200,
                data: {
                    token: token,
                    tokenExpiresIn: process.env.JWT_KEY_LIFE,
                    userName: "User_" + fulfilled._id,
                    userId: fulfilled._id,
                    type: "guest"
                }
            });
        }
    ).catch(error => {
        logger.error('Error.');
        logger.error(error);
        res.status(500).json({
            ok: false,
            message: err,
            data: {}
        });
    })
}

/**
 * This method is used to delete a user
 */

exports.user_delete = (req, res, next) => {
    User.remove({ email: req.params.email })
        .exec()
        .then(result => {
            res.status(200).json({
                ok: true,
                status: 200,
                message: "User deleted",
                data: {}
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                ok: false,
                status: 500,
                message: err,
                data: {}
            });
        });
};

/**
 * This method is used to get user details
 */

exports.user_details = (req, res, next) => {
    const id = req.params.userId;
    console.log("the user id", id);
    User.findById(id)
        .exec()
        .then(result => {
            if (result) {

                res.status(200).json({
                    ok: true,
                    status: 200,
                    message: "User Profile",
                    data: {
                        name: result.name,
                        email: result.email,
                        status: result.status,
                        role: result.userRole,
                        mobile: result.mobile
                    }
                });


            } else {

                res.status(200).json({
                    ok: true,
                    status: 404,
                    message: "No user found",
                    data: {}
                });


            }

        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                ok: false,
                status: 500,
                message: err,
                data: {}
            });
        });
};

/**
 * This method is used to handle
 */
exports.forget_password = (req, res, next) => {

    let searchType = req.body.channel;
    let resetFieldType = req.body.field;
    async.waterfall([
        function (done) {
            crypto.randomBytes(4, function (err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {

            if (searchType === "email") {
                let checkEmail = req.body.value.toLowerCase();
                User.findOne({ email: checkEmail }, function (err, user) {
                    if (!user) {
                        return res.status(200).json({
                            ok: false,
                            status: 404,
                            message: "No account with that email address exists.",
                            data: {}
                        });
                    }

                    user.resetPasswordToken = token;
                    user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                    user.save(function (err) {
                        done(err, token, user);
                    });
                });

            } else {

                let mobileNumber = req.body.value;

                if (mobileNumber.substring(0, 2) === '62') {
                    mobileNumber = '0' + mobileNumber.substring(2);
                }

                User.findOne({ mobile: mobileNumber }, function (err, user) {
                    if (!user) {
                        return res.status(200).json({
                            ok: false,
                            status: 404,
                            message: "No account with this mobile number.",
                            data: {}
                        });
                    }

                    user.resetPasswordToken = token;
                    user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                    user.save(function (err) {
                        done(err, token, user);
                    });
                });

            }

        },
        function (token, user) {
            let resetFieldType = 'password';
            let type = req.body.channel;
            let messageObjEmail = {
                subject: `My Indihome ${resetFieldType} reset`,
                body: `You are receiving this because you  have requested the reset of the ${resetFieldType} for your account.\n\n` +
                    `Please use this secret token to reset the ${resetFieldType}: \n\n` + token + '\n\n' +
                    `If you did not request this, please ignore this email and your ${resetFieldType} will remain unchanged.\n`,

                error: "Error with the email server, Please try again later",
                success: 'Email sent',
                data: {}
            }
            let messageObjSMS = {
                subject: `My Indihome ${resetFieldType} reset`,
                body: `You are receiving this because you  have requested the reset of the ${resetFieldType} for your account.\n\n` +
                    `Please use this secret token to reset the ${resetFieldType}: \n\n` + token + '\n\n' +
                    `If you did not request this, please ignore this SMS and your ${resetFieldType} will remain unchanged.\n`,

                error: "Error with the SMS server, Please try again later",
                success: 'SMS Sent',
                data: {}
            }
            let messageObjWhatsAPP = {
                subject: `my Indihome ${resetFieldType} reset`,
                body: `You are receiving this because you  have requested the reset of the ${resetFieldType} for your account.\n\n` +
                    `Please use this secret token to reset the ${resetFieldType}: \n\n` + token + '\n\n' +
                    `If you did not request this, please ignore this message and your ${resetFieldType} will remain unchanged.\n`,

                error: "Error with the server, Please try again later",
                success: 'Message Sent',
                code: token,
                data: {}
            }

            if (type === 'email') {
                let email = req.body.value;
                notificationService.sendEmail(messageObjEmail, email, res);
            }
            if (type === 'sms') {
                notificationService.getJWT(process.env.CLIENT_GW_USER_NAME, process.env.CLIENT_GW_SECRET, process.env.CLIENT_APP_ID).then(token => {
                    if (token) {
                        notificationService.sendSMS(token, messageObjSMS, req.body.value, res);
                    } else {
                        console.log("the error");
                        res.status(500).json({
                            ok: false,
                            status: 500,
                            message: err,
                            data: {}
                        });
                    }
                });
            }
            if (type === 'whatsApp') {
                notificationService.getJWT(process.env.CLIENT_GW_USER_NAME, process.env.CLIENT_GW_SECRET, process.env.CLIENT_APP_ID).then(token => {
                    if (token) {
                        notificationService.sendWhatsAppSMS(token, messageObjWhatsAPP, req.body.value, res);
                    } else {
                        console.log("the error");
                        res.status(500).json({
                            ok: false,
                            status: 500,
                            message: err,
                            data: {}
                        });
                    }
                });

            }

        }
    ], function (err) {
        return res.status(200).json({
            ok: false,
            status: 406,
            message: "Error, Please try again later",
            data: {}
        });
    });
};


/**
 * This method is used to handle
 */
exports.reset_user_email = (req, res, next) => {
    async.waterfall([
        function (done) {
            crypto.randomBytes(4, function (err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {
            let checkEmail = req.userData.email;
            // validate the email with the blacklisted emails
            if (isBlacklistedEmail(checkEmail)) {
                return res.status(200).json({
                    ok: true,
                    status: 403,
                    message: "blacklisted email address",
                    data: {}
                });
            }
            User.findOne({ email: checkEmail }, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        ok: false,
                        status: 404,
                        message: "No account with that email address exists.",
                        data: {}
                    });
                }

                user.resetEmailToken = token;
                user.resetEmailExpires = Date.now() + 3600000; // 1 hour

                user.save(function (err) {
                    done(err, token, user);
                });
            });
        },
        function (token, user) {
            let resetFieldType = 'email address';
            let messageObjEmail = {
                subject: `My Indihome ${resetFieldType} reset`,
                body: `You are receiving this because you  have requested the reset of the ${resetFieldType} for your account.\n\n` +
                    `Please use this secret token to reset the ${resetFieldType}: \n\n` + token + '\n\n' +
                    `If you did not request this, please ignore this email and your ${resetFieldType} will remain unchanged.\n`,

                error: "Error with the email server, Please try again later",
                success: 'Email sent',
                data: {}
            }
            let email = req.body.value;
            notificationService.sendEmail(messageObjEmail, email, res);
        }
    ], function (err) {
        return res.status(200).json({
            ok: false,
            status: 406,
            message: "Error, Please try again later",
            data: {}
        });
    });
};


/**
 * This method is used to handle
 * 1. user forget password: a token will be sent to user email
 * 2. user change email: a token will be sent to user email/ mobile based on his preference
 * 3. user change mobile: a token will be sent to user email/ mobile based on his preference
 */
exports.reset_user_mobile = (req, res, next) => {

    async.waterfall([
        function (done) {
            crypto.randomBytes(4, function (err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {

            let mobileNumber = req.userData.mobile;
            if (mobileNumber.substring(0, 2) === '62') {
                mobileNumber = '0' + mobileNumber.substring(2);
            }

            User.findOne({ mobile: mobileNumber }, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        ok: false,
                        status: 404,
                        message: "No account with this mobile number.",
                        data: {}
                    });
                }

                user.resetMobileToken = token;
                user.resetMobileExpires = Date.now() + 3600000; // 1 hour

                user.save(function (err) {
                    done(err, token, user);
                });
            });
        },
        function (token, user) {
            let resetFieldType = 'mobile number';
            let type = req.body.channel;
            let messageObjSMS = {
                subject: `My Indihome ${resetFieldType} reset`,
                body: `You are receiving this because you  have requested the reset of the ${resetFieldType} for your account.\n\n` +
                    `Please use this secret token to reset the ${resetFieldType}: \n\n` + token + '\n\n' +
                    `If you did not request this, please ignore this SMS and your ${resetFieldType} will remain unchanged.\n`,

                error: "Error with the SMS server, Please try again later",
                success: 'SMS Sent',
                data: {}
            }
            let messageObjWhatsAPP = {
                subject: `my Indihome ${resetFieldType} reset`,
                body: `You are receiving this because you  have requested the reset of the ${resetFieldType} for your account.\n\n` +
                    `Please use this secret token to reset the ${resetFieldType}: \n\n` + token + '\n\n' +
                    `If you did not request this, please ignore this message and your ${resetFieldType} will remain unchanged.\n`,

                error: "Error with the server, Please try again later",
                success: 'Message Sent',
                code: token,
                data: {}
            }
            if (type === 'sms') {
                notificationService.getJWT(process.env.CLIENT_GW_USER_NAME, process.env.CLIENT_GW_SECRET, process.env.CLIENT_APP_ID).then(token => {
                    if (token) {
                        notificationService.sendSMS(token, messageObjSMS, req.body.value, res);
                    } else {
                        console.log("the error");
                        res.status(500).json({
                            ok: false,
                            status: 500,
                            message: err,
                            data: {}
                        });
                    }
                });
            }
            if (type === 'whatsApp') {
                notificationService.getJWT(process.env.CLIENT_GW_USER_NAME, process.env.CLIENT_GW_SECRET, process.env.CLIENT_APP_ID).then(token => {
                    if (token) {
                        notificationService.sendWhatsAppSMS(token, messageObjWhatsAPP, req.body.value, res);
                    } else {
                        console.log("the error");
                        res.status(500).json({
                            ok: false,
                            status: 500,
                            message: err,
                            data: {}
                        });
                    }
                });
            }
        }
    ], function (err) {
        return res.status(200).json({
            ok: false,
            status: 406,
            message: "Error, Please try again later",
            data: {}
        });
    });
};

const verifyVerificationJoi = joi.object({
    recoveryCode: joi.string().required(),
    channel: joi.string().required(),
    value: joi.string().required()
});

/**
 *  verify recovery code
 */
exports.verify_recovery_code = async (req, res, next) => {

    const payload = req.body;
    try {
        const value = await verifyVerificationJoi.validateAsync(payload);
    }
    catch (err) {
        return res.status(200).json({
            ok: false,
            status: 400,
            message: "'error', 'Invalid payload.",
            data: {}
        });
    }
    let query;
    if(payload.channel === 'sms' || payload.channel === 'whatsapp') {
        query = {
            resetPasswordToken: payload.recoveryCode,
            resetPasswordExpires: {$gt: Date.now()},
            mobile: payload.value
        }
    }else{
        query = {
            resetPasswordToken: payload.recoveryCode,
            resetPasswordExpires: {$gt: Date.now()},
            email: payload.value
        }
    }

    async.waterfall([
        function (done) {
            User.findOne(query, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        ok: false,
                        status: 401,
                        message: "'error', 'Password reset token is invalid or has expired.",
                        data: {}
                    });
                }

                return res.status(200).json({
                    ok: true,
                    status: 200,
                    message: "valid recovery token",
                    data: {}
                });
            });
        },
    ], function (err) {
        return res.status(200).json({
            ok: false,
            status: 406,
            message: "Error, Please try again later",
            data: {}
        });
    });
};

/**
 *  Reset password method
 */
exports.reset_password = (req, res, next) => {

    async.waterfall([
        function (done) {
            User.findOne({
                resetPasswordToken: req.body.resetPasswordToken,
                resetPasswordExpires: { $gt: Date.now() }
            }, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        ok: false,
                        status: 401,
                        message: "'error', 'Password reset token is invalid or has expired.",
                        data: {}
                    });
                }
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            ok: false,
                            status: 500,
                            message: err,
                            data: {}
                        });
                    } else {
                        user.password = hash;
                        user.resetPasswordToken = undefined;
                        user.resetPasswordExpires = undefined;
                        user.save().then(result => {
                            console.log(result);
                            res.status(200).json({
                                ok: true,
                                status: 200,
                                message: "Password created",
                                data: {}
                            });
                        }).catch(err => {
                            console.log(err);
                            res.status(500).json({
                                ok: false,
                                status: 200,
                                message: err,
                                data: {}
                            });
                        });
                    }
                });
            });
        },
    ], function (err) {
        return res.status(200).json({
            ok: false,
            status: 406,
            message: "Error, Please try again later",
            data: {}
        });
    });
};

/**
 *  new password method
 */
exports.new_password = (req, res, next) => {

    async.waterfall([
        function (done) {

            let emailPassed = req.body.email.toLowerCase();
            User.findOne({
                email: emailPassed
            }, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        ok: false,
                        status: 404,
                        message: "'error', 'No user found.",
                        data: {}
                    });
                }

                bcrypt.compare(req.body.password, user.password, (err, result) => {
                    if (err) {
                        return res.status(200).json({
                            ok: false,
                            status: 401,
                            message: "Wrong login credentials",
                            data: {}

                        });
                    }
                    if (result) {

                        bcrypt.hash(req.body.newPassword, 10, (err, hash) => {
                            if (err) {
                                return res.status(500).json({
                                    ok: false,
                                    status: 500,
                                    message: err,
                                    data: {}
                                });
                            } else {
                                user.password = hash;
                                user.resetPasswordToken = undefined;
                                user.resetPasswordExpires = undefined;
                                user.save().then(result => {
                                    console.log(result);
                                    res.status(200).json({
                                        ok: true,
                                        status: 200,
                                        message: "Password created",
                                        data: {}
                                    });
                                }).catch(err => {
                                    console.log(err);
                                    res.status(500).json({
                                        ok: false,
                                        status: 200,
                                        message: err,
                                        data: {}
                                    });
                                });
                            }
                        });
                    } else {
                        return res.status(200).json({
                            ok: false,
                            status: 401,
                            message: "Wrong login credentials",
                            data: {}

                        });
                    }
                }
                );
            });
        },
    ], function (err) {
        return res.status(200).json({
            ok: false,
            status: 406,
            message: "Error, Please try again later",
            data: {}
        });
    });
};

/**User activation */
exports.user_activation = (req, res, ) => {
    let emailAdress = req.body.email;
    async.waterfall([
        function (done) {
            User.findOne({
                userActiveToken: req.body.userActiveToken
            }, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        ok: false,
                        status: 401,
                        message: "'error', 'Password reset token is invalid or has expired.",
                        data: {}
                    });
                }
                user.status = "active";
                user.userActiveToken = undefined;
                user.save().then(result => {
                    console.log(result);
                    res.status(200).json({
                        ok: true,
                        status: 200,
                        message: "user activated",
                        data: {}
                    });
                }).catch(err => {
                    console.log(err);
                    res.status(500).json({
                        ok: false,
                        status: 500,
                        message: err,
                        data: {}
                    });
                });
            });
        },
    ], function (err) {
        return res.status(200).json({
            ok: true,
            status: 406,
            message: "Error, Please try again later",
            data: {}
        });
    });
};


//user blocking
exports.user_blocking = (req, res, ) => {
    let emailAdress = req.body.email.toLowerCase();
    async.waterfall([
        function (done) {
            User.findOne({
                email: emailAdress
            }, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        ok: false,
                        status: 401,
                        message: "'error', 'No user registered with this Email.",
                        data: {}
                    });
                }
                user.status = "locked";
                user.save().then(result => {
                    console.log(result);
                    res.status(200).json({
                        ok: true,
                        status: 403,
                        message: "user locked",
                        data: {}
                    });
                }).catch(err => {
                    console.log(err);
                    res.status(500).json({
                        ok: false,
                        status: 500,
                        message: err,
                        data: {}
                    });
                });
            });
        },
    ], function (err) {
        return res.status(200).json({
            ok: false,
            status: 496,
            message: "Error, Please try again later",
            data: {}
        });
    });
};

/**
 *  Update email method
 */
exports.change_user_email = (req, res, next) => {

    async.waterfall([
        function (done) {
            const currenTime = Date.now();
            User.findOne({
                resetEmailToken: req.body.resetEmailToken,
                resetEmailExpires: { $gt: currenTime }
            }, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        ok: false,
                        status: 401,
                        message: "'error', 'Email reset token is invalid or has expired.",
                        data: {}
                    });
                }

                let newEmail = req.body.value;
                if (newEmail) {
                    user.email = newEmail;
                    user.resetEmailToken = undefined;
                    user.resetEmailExpires = undefined;
                    user.save().then(result => {
                        console.log(result);
                        res.status(200).json({
                            ok: true,
                            status: 200,
                            message: "Email updated",
                            data: {}
                        });
                    }).catch(err => {
                        console.log(err);
                        res.status(500).json({
                            ok: false,
                            status: 200,
                            message: err,
                            data: {}
                        });
                    });
                }
                else {
                    return res.status(200).json({
                        ok: false,
                        status: 400,
                        message: "'error', 'Invalid email provided.",
                        data: {}
                    });
                }
            });
        },
    ], function (err) {
        return res.status(200).json({
            ok: false,
            status: 406,
            message: "Error, Please try again later",
            data: {}
        });
    });
};

/**
 *  Update mobile method
 */
exports.change_user_mobile = (req, res, next) => {

    async.waterfall([
        function (done) {
            User.findOne({
                resetMobileToken: req.body.resetMobileToken,
                resetMobileExpires: { $gt: Date.now() }
            }, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        ok: false,
                        status: 401,
                        message: "'error', 'Mobile reset token is invalid or has expired.",
                        data: {}
                    });
                }

                let mobileNumber = req.body.value;
                if (mobileNumber.substring(0, 2) === '62') {
                    mobileNumber = '0' + mobileNumber.substring(2);
                }

                if (mobileNumber) {
                    user.mobile = mobileNumber;
                    user.resetEmailToken = undefined;
                    user.resetEmailExpires = undefined;
                    user.save().then(result => {
                        console.log(result);
                        res.status(200).json({
                            ok: true,
                            status: 200,
                            message: "Mobile Number updated",
                            data: {}
                        });
                    }).catch(err => {
                        console.log(err);
                        res.status(500).json({
                            ok: false,
                            status: 200,
                            message: err,
                            data: {}
                        });
                    });
                }
                else {
                    return res.status(200).json({
                        ok: false,
                        status: 400,
                        message: "'error', 'Invalid mobile number.",
                        data: {}
                    });
                }
            });
        },
    ], function (err) {
        return res.status(200).json({
            ok: false,
            status: 406,
            message: "Error, Please try again later",
            data: {}
        });
    });
};



/**
 *  Update Name method
 */
exports.change_user_name = (req, res, next) => {

    async.waterfall([
        function (done) {

            let emailAdress = req.userData.email.toLowerCase();
            User.findOne({
                email: emailAdress
            }, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        ok: false,
                        status: 401,
                        message: "'error', 'User not found",
                        data: {}
                    });
                }

                user.name = req.body.name;
                user.save().then(result => {
                    console.log(result);
                    res.status(200).json({
                        ok: true,
                        status: 200,
                        message: "Name changed",
                        data: {}
                    });
                }).catch(err => {
                    console.log(err);
                    res.status(500).json({
                        ok: false,
                        status: 200,
                        message: err,
                        data: {}
                    });
                });
            });
        },
    ], function (err) {
        return res.status(200).json({
            ok: false,
            status: 406,
            message: "Error, Please try again later",
            data: {}
        });
    });
};


/**
 * This method is used to Save an User by a service
 */
exports.user_save = (req, res, next) => {

    let emailIn = req.body.email.toLowerCase();
   User.find({ email: emailIn })
       .exec()
       .then(user => {
           if (user.length >= 1) {
            return res.status(200).json({
                   ok: false,
                   status: 409,
                   message: "Email already registered",
                   data: {}
               });
           }
           else {

               User.find({ mobile: req.body.mobile+""})
                   .exec()
                   .then(user => {
                       if (user.length >= 1) {

                           logger.info('Mobile Number already registered.');
                           logger.info( req.body.mobile);
                           return res.status(200).json({
                               ok: false,
                               status: 410,
                               message: "Mobile Number already registered",
                               data: {}

                           });
                       } else {
                           bcrypt.hash(req.body.password, 10, (err, hash) => {
                               if (err) {
                                   logger.info('Error.');
                                   logger.info(err);

                                   return res.status(500).json({
                                       ok: false,
                                       status: 500,
                                       message: err,
                                       data: {}
                                   });
                               } else {
                                   var tokenUserActive = crypto.randomBytes(20).toString('hex');

                                   let emailAdress = req.body.email.toLowerCase();
                                   const user = new User({
                                       _id: new mongoose.Types.ObjectId(),
                                       email: emailAdress,
                                       name: req.body.name,
                                       password: hash,
                                       mobile: req.body.mobile,
                                       userRole: [req.body.userRole],
                                       indiHomeNumber: null,
                                       refCode: req.body.refCode,
                                       status: req.body.status,
                                       userActiveToken: tokenUserActive,
                                       resetPasswordToken: req.body.resetPasswordToken,
                                       resetPasswordExpires: req.body.resetPasswordExpires
                                   });
                                   user
                                       .save()
                                       .then(result => {
                                           logger.info('User created.');
                                           logger.info(JSON.stringify(result));
                                           res.status(201).json({
                                               ok: true,
                                               status: 201,
                                               message: "User created",
                                               data: { tokenUserActive: tokenUserActive }
                                           });
                                       })
                                       .catch(err => {
                                           logger.error('Error.');
                                           logger.error(err);
                                           res.status(500).json({
                                               ok: false,
                                               status: 500,
                                               message: err,
                                               data: {}
                                           });
                                       });
                               }
                           });
                       }
                   });

           }
       })
       .catch(err => {

           logger.error('Error.');
           logger.error(err);

           res.status(500).json({
               ok: false,
               status: 500,
               message: err,
               data: {}
           });
       });

};
