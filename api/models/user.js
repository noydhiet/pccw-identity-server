const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: { 
        type: String, 
        required: true,
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password: { type: String, required: true },
    name: { type: String, required: true },
    mobile: { type: String, required: true , unique: true },
    indiHomeNumber: { type: String, required: false , unique: false },
    userRole: { type:[String], required: true },
    status: { type: String, required: true },
    refCode:String,
    userActiveToken: String,
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    resetEmailToken: String,
    resetEmailExpires: Date,
    resetMobileToken: String,
    resetMobileExpires: Date
});

module.exports = mongoose.model('User', userSchema);
