const mongoose = require('mongoose');

const otpSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,

    token: { type: String, required: true },
    code: { type: String, required: true },
    isChecked: { type: Boolean, default: false },
    attempts: { type: Number, default: 3 },
    checkDate: null,
    creationDate: { type: Date, default: new Date() }
});

module.exports = mongoose.model('Otp', otpSchema);