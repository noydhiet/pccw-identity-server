const mongoose = require('mongoose');

const guestSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId, 
    deviceType: { type: String},
    creationDate: { type: Date, default: new Date() }
});

module.exports = mongoose.model('Guest', guestSchema);
