const mongoose = require('mongoose');

const mobileSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    mobile: { type: String, required: true , unique: false },
    email: { type: String, required: true , unique: false },
    key: { type: String, required: true , unique: true },
    attemptsRemaining: { type: Number, default: 3 },
    userBlockedAt:{ type: Date, default: new Date() },
    token: { type: String, required: false },
    code: { type: String, required: true },
    isChecked: { type: Boolean, default: false },
    checkDate: Date,
    creationDate: { type: Date, default: new Date() }
});

module.exports = mongoose.model('otp', mobileSchema);