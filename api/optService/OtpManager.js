const OtpItem = require("./OtpItem");
const Mobile = require("../models/mobile");
const mongoose = require("mongoose");
const mobileService = require("../services/mobileService");
const otpRepository = require("../optService/otpRepository");

//Logging
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'info';

const VerificationResults = {
  valid: Symbol("valid"),
  notValid: Symbol("notValid"),
  expired: Symbol("expired"),
  checked: Symbol("checked"),
  blocked: Symbol("blocked"),
  error: Symbol("error"),
};


class OtpManager {
  constructor(otpRepository, options) {
    this.VerificationResults = VerificationResults;
    this.options = options || { otpLength: 4, validityTime: 3 };
  }

  create(token, req, creatingOTP,res) {
    mobileService.isUserBlocked(req).then(
      fulfilled => {
        const code = Math.floor(1000 + Math.random() * 9000).toString();
        let otp = new OtpItem(token, code);
        mobileService.saveOTP(req,3,code,otp,res,creatingOTP);
      }
    )
      .catch(error => {
        if (error == 500) {
          creatingOTP(500,req,res);
        } else {
          creatingOTP(402,req,res);
        }
      });
  }

  verify(token, code, req, res,verifyOTPResult) { 
    let otp = null;

    mobileService.getOTP(req).then(
      fulfilled => {
         otp = fulfilled;
      }
      ).catch(error => {
      otp = error;
      }).finally(function() {
        let verificationResult = VerificationResults.notValid;
        let remainingAttempts = 2;
        mobileService.noOFAttempts(req).then(
          fulfilled => {
           remainingAttempts = fulfilled;
          }
           ).catch(error => {
            remainingAttempts = 0;
            logger.error('Error.');
            logger.error(err);
          }).finally(function() {
            if(otp !== null)
            {
             if (remainingAttempts > 0) {
               remainingAttempts = remainingAttempts - 1;
               mobileService.updateAttepts(req,remainingAttempts);
               if (otp) {
                 switch (true) {
                   case otp.isChecked:
                     verificationResult = VerificationResults.checked;
                     break;
                   case isOtpExpired(otp,3):
                     verificationResult = VerificationResults.expired;
                     if(remainingAttempts == 0){
                      verificationResult = VerificationResults.blocked;
                      mobileService.blockMobileOTP(req);
                     }
                     break;
                   default:
                      if(otp.code == code){
                        otp.isChecked = true;
                        otp.checkDate = new Date();
                        mobileService.updateOTP(otp,3);
                        verificationResult = VerificationResults.valid;
                      }else{
                        verificationResult = VerificationResults.notValid
                        if(remainingAttempts == 0){
                         verificationResult = VerificationResults.blocked;
                         mobileService.blockMobileOTP(req);
                        }
                      }  
                 }
                 verifyOTPResult(req,res,VerificationResults,verificationResult); 
               }
             }
             else {
               Mobile.findOne({ key: req.body.mobile+req.body.email }, function (err, mobile) {
                   mobile.userBlockedAt = new Date;
                   mobile
                     .save()
                     .then(result => {
                       logger.info('User Blocked.');
                       logger.info(JSON.stringify(result));
                       verificationResult = VerificationResults.blocked;
                       mobileService.blockMobileOTP(req);
                       verifyOTPResult(req,res,VerificationResults,verificationResult);
                     })
                     .catch(err => {
                       logger.error('Error.');
                       logger.error(err);
                       verificationResult = VerificationResults.error;
                       verifyOTPResult(req,res,VerificationResults,verificationResult);
                     });
             });
             }
           }else{
            verificationResult = VerificationResults.notValid;
             remainingAttempts = remainingAttempts - 1;
              mobileService.updateAttepts(req,remainingAttempts);
              
             if (remainingAttempts < 0) {
    
              Mobile.findOne({ mobile: req.body.mobile + "" }, function (err, mobile) {
                mobile.userBlockedAt = new Date;
                mobile
                  .save()
                  .then(result => {
                    logger.info('User Blocked.');
                    logger.info(JSON.stringify(result));
                    verificationResult = VerificationResults.blocked;
                    mobileService.blockMobileOTP(req);
                    verifyOTPResult(req,res,VerificationResults,verificationResult);
                  })
                  .catch(err => {
                    logger.error('Error.');
                    logger.error(err);
                    verificationResult = VerificationResults.error;
                    verifyOTPResult(req,res,VerificationResults,verificationResult);
                  });
          });
    
             }else{
              logger.error('No OTP.');
              verificationResult = VerificationResults.notValid;
              verifyOTPResult(req,res,VerificationResults,verificationResult);
             }
           }
            
        });
      }
      );
  }
}

function isOtpExpired(otp, validityTime) {
  const minutesSinceCreation = ((new Date() - new Date(otp.creationDate)) % 3.6e6) / 6e4;
  return (minutesSinceCreation > validityTime);
}


module.exports = OtpManager;
``