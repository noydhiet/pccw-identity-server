const util = require("util");
var nodemailer = require('nodemailer');
const request = require("request-promise");
const sendEmailApi = process.env.CLIENT_EMAIL_API;
const emailSender= process.env.CLIENT_EMAIL_SENDER;

const log4js = require('log4js');
let logger = log4js.getLogger('Otp');
logger.level = 'info';


/**
 * Get access token form the cline API useing Basic Authentication
 *
 * user String username
 * secret String password
 **/
async function getJWT(user, secret, app_id) {
  let options = {
    method: 'GET',
    // fixme: config
    url: process.env.CLIENT_SG_URL,
    qs: { app_id: app_id },
    headers:
    {
      Authorization: 'Basic ' + Buffer.from(user + ':' + secret).toString('base64'),
      Accept: 'application/json',
      "Content-Type": "application/json"
    },
    json: true,
    strictSSL: false
  };

  let resp = await request(options).catch(err => {
    console.log("error");
    res.status(500).json({
      ok: false,
      status: 500,
      message:"Core Server Error",
      data: {}
  });

  });
  if (!util.isNullOrUndefined(resp)) {
    console.log(resp.jwt);
    return resp.jwt;
  }
  else {
    return null;
  }

}


async function sendSMS(jwt, message, mobile,res,attemps) {

  var options = {
    method: 'POST',
    url: process.env.CLIENT_SMS_API,
    headers:
    {
      authorization: 'Bearer ' + jwt,
      'content-type': 'application/json'
    },
    body:
    {
      senderid: 'INDIHOME',
      username: 'test',
      password: 'test',
      messageType: 'OTP',
      msisdn: [mobile],
      message: message
    },
    json: true,
    strictSSL: false
  };

  let resp = await request(options).catch(err => {
    console.log(err);
    res.status(500).json({
      ok: false,
      status: 500,
      message:"Error sending SMS",
      data: {}
  });

  });
  if (!util.isNullOrUndefined(resp)) {

        if(resp.data.responses.response.code == 1){

          res.status(200).json({
            ok: true,
            status:200,
            message: "OTP Sent",
            data:{ validityTime: 3,
                   noOfAttempts: attemps,}
        });

        }
        else{

          if(resp.data.responses.response.code == 0){

            res.status(200).json({
              ok: false,
              status: 401,
              message: "Invalid mobile number",
              data: {}
          });
          }else{
            res.status(500).json({
              ok: false,
              status: 500,
              message: "Error sending SMS",
              data: {}
          });
          }
        }
  }
  else {
    console.log("error");
    res.status(500).json({
      ok: false,
      status: 500,
      message: "Server Error",
      data: {}
  });

  }

}


async function sendWhatsAppSMS(jwt, message, mobile,res,attemps,code) {

  var templateData = {
		data1: code
  }

  let mobileNumber = mobile;

  if(mobile.charAt(0) === '0'){
    mobileNumber = mobile.substr(1);
    mobileNumber =  "62"+ mobileNumber;
  }

  var options = {
    method: 'POST',
    url: process.env.CLIENT_WHATSAPP_API,
    headers:
    {
      authorization: 'Bearer ' + jwt,
      'content-type': 'application/json'
    },
    body:
    {
      ticketID: '',
      source: 'OTP',
      templateID: 'otp',
      phone: mobileNumber,
      templateData:templateData,
      smsText: message
    },
    json: true,
    strictSSL: false
  };

  let resp = await request(options).catch(err => {
    console.log("error");
    res.status(500).json({
      ok: false,
      status: 500,
      message:"Error sending whatApp",
      data: {}
  });

  });

  if (!util.isNullOrUndefined(resp)) {

        if(resp.statusCode == 0){

          res.status(200).json({
            ok: true,
            status:200,
            message: "OTP Sent",
            data:{ validityTime: 3,
                   noOfAttempts: attemps,}
        });

        }
        else{

          if(resp.statusCode == 1){

            res.status(200).json({
              ok: false,
              status: 401,
              message: "Error sending WhatApp",
              data: {}
          });
          }else{
            res.status(500).json({
              ok: false,
              status: 500,
              message: "Error sending WhatApp",
              data: {}
          });
          }
        }
  }
  else {
    console.log("error");
    res.status(500).json({
      ok: false,
      status: 500,
      message: "Error sending WhatApp",
      data: {}
  });

  }

}

async function sendEmail(message, recipientAdresses, res, attemps) {

  const authorization = `Bearer ${await getJWT(process.env.CLIENT_GW_USER_NAME,
      process.env.CLIENT_GW_SECRET, process.env.CLIENT_APP_ID)}`;
  const json = {
    addressTO: '' + recipientAdresses.email,
    addressBCC: '',
    addressFROM: emailSender,
    addressCC: '',
    messageBody: message,
    subject: 'my Indihome OTP'
  };
  const options = {
    method: 'POST',
    uri: sendEmailApi,
    headers: {
      'Content-Type': 'application/json',
      Authorization: authorization,
    },
    json,
    strictSSL: false
  };

  const response = await request.post(options).catch(e => {
    logger.error('send email error', e);
    return res.status(500).json({
      ok: false,
      status: 500,
      message: "Error with the email server, Please try again later",
      data: {}
    });
  });
  if (response && response.statusCode === "0") {
    res.status(200).json({
      ok: true,
      status: 200,
      message: "OTP Sent",
      data: {
        validityTime: 3,
        noOfAttempts: attemps
      }
    });
  } else {
    res.status(500).json({
      ok: false,
      status: 500,
      message: "Error with the email server, Please try again later",
      data: {}
    });
  }
}

async function sendEmailGoogle(message, recipientAdresses,res,attemps) {

  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.EMAIL_SENDER,
        pass: process.env.EMAIL_PW
    }
});
var mailOptions = {
    from: process.env.EMAIL_SENDER,
    to: '' + recipientAdresses.email,
    subject: 'my Indihome OTP',
    text: message
};
transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
        return res.status(500).json({
            ok: false,
            status: 500,
            message: "Error with the email server, Please try again later",
            data: {}
        });
    } else {


      res.status(200).json({
        ok: true,
        status:200,
        message: "OTP Sent",
        data:{ validityTime: 3,
               noOfAttempts: attemps,}
    });

    }
});

}


function send(otp, recipientAdresses,res) {

  var message = "Please use this OTP " + otp.code + " to proceed with the application"

  try {

    getJWT(process.env.CLIENT_GW_USER_NAME,process.env.CLIENT_GW_SECRET,process.env.CLIENT_APP_ID).then(token => {

      if (token) {

      let channel = recipientAdresses.channel;

       if(channel ==='email'){
        sendEmail(message,recipientAdresses,res, otp.attempts);

       }
       else if(channel ==='sms'){
        sendSMS(token, message, recipientAdresses.mobile,res, otp.attempts);
       }
       else if(channel ==='whatsApp'){

        sendWhatsAppSMS(token, message, recipientAdresses.mobile,res, otp.attempts, otp.code);

      }
       else {
         console.log("invalid channel");
         res.status(200).json({
           ok: false,
           status: 400,
           message: `invalid channel: ${channel}`,
           data: {}
         });
       }
      } else {
        console.log("the error");
        res.status(500).json({
          ok: false,
          status: 500,
          message: err,
          data: {}
      });
      }
    });

  } catch (error) {
    console.error(error);
    res.status(500).json({
      ok: false,
      status: 500,
      message: err,
      data: {}
  });
  }

}


function convertMobile(mobile){
  let s1 = mobile;
  let s2 = s1.substr(1) + "62";
  return s2;
}

module.exports = {
  send
};
