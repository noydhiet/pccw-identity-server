/**
 * This method is for the guest stats
 */


const Guest = require("../models/guest");
const mongoose = require("mongoose");

//Logging
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'info'


function saveNewGuest(req) {

    return new Promise(
        function (resolve, reject) {
    const guestItem = new Guest({
      _id: new mongoose.Types.ObjectId(),
      deviceType:req.body.deviceType,
      creationDate: new Date()   
    });
    guestItem
      .save()
      .then(result => {
        logger.info('Guest user created.');
        logger.info(JSON.stringify(result));
        resolve(result);
      })
      .catch(err => {
        logger.error('Error.');
        logger.error(err);
        reject(err);
      });
    });
    }
  
  
    module.exports = {
        saveNewGuest
      };