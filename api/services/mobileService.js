const Mobile = require("../models/mobile");
const mongoose = require("mongoose");

//Logging
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'info';

const saveStatus = {
    valid: Symbol("done"),
    notValid: Symbol("error"),
  };


function saveNewOTP(req,attempts,code,otp,res,creatingOTP) {
  const mobileItem = new Mobile({
    _id: new mongoose.Types.ObjectId(),
    mobile: req.body.mobile,
    email:req.body.email,
    key:req.body.mobile+req.body.email,
    userBlockedAt:null,
    attemptsRemaining:attempts,
    creationDate:new Date(),
    code:code
  });
  mobileItem
    .save()
    .then(result => {
      logger.info('new OTP created.');
      result.code = '*****';  //code is modified to a dummy text since it
      logger.info(JSON.stringify(result));
      creatingOTP(otp,req,res);
    })
    .catch(err => {
      logger.error('Error.');
      logger.error(err);
      res.status(500).json({
        ok: false,
        status: 500,
        message:"Core Server Error",
        data: {}
       });

    });
}


function saveOTP(req,attempts,code,otp,res,creatingOTP) {

  let keyin = req.body.mobile+req.body.email;

  Mobile.findOne({ key: keyin }, function (err, mobile) {
    if(mobile !== null){
        mobile.attemptsRemaining = attempts;
        mobile.code = code;
        mobile.isChecked = false;
        mobile.creationDate = new Date();
        mobile.attemptsRemaining = attempts;
        mobile
          .save()
          .then(result => {
            logger.info('OTP created.');
            result.code = '*****';
            logger.info(JSON.stringify(result));
            creatingOTP(otp,req,res);
          })
          .catch(err => {
            logger.error('Error.');
            logger.error(err);

            res.status(500).json({
              ok: false,
              status: 500,
              message:"Core Server Error",
              data: {}
             });

          });
    
    }else{
      saveNewOTP(req,3,code,otp,res,creatingOTP);
    }
    });
}

function blockMobileOTP(req){
  let keyin = req.body.mobile+req.body.email;

  Mobile.findOne({ key: keyin }, function (err, mobile) {
    if (mobile !== null) {
      mobile.userBlockedAt = new Date;
      mobile
        .save()
        .then(result => {
          logger.info('User Blocked.');
          logger.info(JSON.stringify(result));
        })
        .catch(err => {
          logger.error('Error.');
          logger.error(err);
        });
    } 
});

}


function updateAttepts(req,attempts){

  Mobile.findOne({key: req.body.mobile+req.body.email }, function (err, mobile) {
    if(mobile !== null){
    
        mobile.attemptsRemaining = attempts;
        mobile
          .save()
          .then(result => {
            logger.info('User Updated.');
            logger.info(JSON.stringify(result));
          })
          .catch(err => {
            logger.error('Error.'); 
          });
     
    }

}).catch(err => {
  logger.error('Error.');
  logger.error(err);

});

}

function getAttepts(mobileNo){

  Mobile.findOne({ mobile: mobileNo + "" }, function (err, mobile) {
    if(mobile !== null){
     if (mobile.length >= 1) {
          let attempts = mobile.attemptsRemaining;
          return attempts;
      }  
    }else{
      logger.error('Error.'); 
       return 3;
    }

}).catch(err => {
  logger.error('Error.');
  logger.error(err);

});

}

function noOFAttempts(req) {
  return new Promise(
      function (resolve, reject) {
          Mobile.find({ key: req.body.mobile+req.body.email })
            .exec()
            .then(mobile => {
              if (mobile.length >= 1) {
                resolve(mobile[0].attemptsRemaining);
              } else {
                console.log("NO account");
                updateAttepts(req,3);
                resolve(3);
              }
            }).catch(err => {
              reject(500);
              logger.error('Error.');
              logger.error(err);
            });
    
        }
  );
}

function diff_hours(dt2, dt1) {
  var diff = (dt2.getTime() - dt1.getTime()) / 1000;
  diff /= (60 * 60);
  return Math.abs(diff);
}

function isUserBlocked(req) {

  return new Promise(
    function (resolve, reject) {
      Mobile.find({ key: req.body.mobile+req.body.email })
        .exec()
        .then(mobile => {
          if (mobile.length >= 1) {

            if(mobile[0].userBlockedAt == null){
              resolve(1);
            }else{
              let timeDiff = diff_hours(mobile[0].userBlockedAt, new Date);
              if (timeDiff <= 1) {

                reject(0);
  
              } else {
                resolve(1);
              }
            }
        
          } else {
            console.log("NO account");
            resolve(1);
          }
        }).catch(err => {

          reject(500);
          logger.error('Error.');
          logger.error(err);
        });

    }
  );

}


function getOTP(req){
  return new Promise(
    function (resolve, reject) {
      Mobile.findOne({ key: req.body.mobile+req.body.email }, function (err, mobile) {
        if(mobile !== null){
      
         resolve(mobile)
        }else{
          reject(null);
        }
    
    }).catch(err => {
      logger.error('Error.');
      logger.error(err);
      reject(null);
    });
    });
  }


function updateOTP(otp,attempts) {
  Mobile.findOne({ key: otp.key }, function (err, mobile) {
if(mobile !== null){
    mobile.attemptsRemaining = attempts;
    mobile.code = otp.code;
    mobile.checkDate = otp.checkDate;
    mobile.isChecked = otp.isChecked;
    mobile
      .save()
      .then(result => {
        logger.info('OTP created.');
        logger.info(JSON.stringify(result));
      })
      .catch(err => {
        logger.error('Error.');
        logger.error(err);
     
      });
}
});
}



module.exports = {
  saveNewOTP,getAttepts,updateAttepts,blockMobileOTP,saveOTP,saveNewOTP,noOFAttempts,isUserBlocked,getOTP,updateOTP
};

