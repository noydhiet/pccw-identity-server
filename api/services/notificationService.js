const util = require("util");
var nodemailer = require('nodemailer');
const request = require("request-promise");
const sendEmailApi = process.env.CLIENT_EMAIL_API;
const emailSender= process.env.CLIENT_EMAIL_SENDER;

const log4js = require('log4js');
let logger = log4js.getLogger('Notifications');
logger.level = 'info';

/**
 * Get access token form the cline API useing Basic Authentication
 *
 * user String username
 * secret String password
 **/
async function getJWT(user, secret, app_id) {
    let options = {
      method: 'GET',
      // fixme: config
      url: process.env.CLIENT_SG_URL,
      qs: { app_id: app_id },
      headers:
      {
        Authorization: 'Basic ' + Buffer.from(user + ':' + secret).toString('base64'),
        Accept: 'application/json',
        "Content-Type": "application/json"
      },
      json: true,
      strictSSL: false
    };

    let resp = await request(options).catch(err => {
      console.log("error");
      res.status(500).json({
        ok: false,
        status: 500,
        message:"Core Server Error",
        data: {}
    });

    });
    if (!util.isNullOrUndefined(resp)) {
      console.log(resp.jwt);
      return resp.jwt;
    }
    else {
      return null;
    }

  }

async function sendEmail(messageObj, recipientAdresses, res) {
  const authorization = `Bearer ${await getJWT(process.env.CLIENT_GW_USER_NAME,
      process.env.CLIENT_GW_SECRET, process.env.CLIENT_APP_ID)}`;
  const json = {
    addressTO: recipientAdresses,
    addressBCC: '',
    addressFROM: emailSender,
    addressCC: '',
    messageBody: messageObj.body,
    subject: messageObj.subject
  };
  const options = {
    method: 'POST',
    uri: sendEmailApi,
    headers: {
      'Content-Type': 'application/json',
      Authorization: authorization,
    },
    json,
    strictSSL: false
  };

  const response = await request.post(options).catch(e => {
    logger.error('send email error', e);
    return res.status(500).json({
      ok: true,
      status: 500,
      message: messageObj.error,
      data: {}
    });
  });
  if (response && response.statusCode === "0") {
    return res.status(200).json({
      ok: true,
      status: 200,
      message: messageObj.success,
      data: messageObj.data
    });
  } else {
    return res.status(500).json({
      ok: true,
      status: 500,
      message: messageObj.error,
      data: {}
    });
  }
}


/**
 * @deprecated
 * @param messageObj
 * @param recipientAdresses
 * @param res
 * @returns {Promise<void>}
 */
async function sendEmailGoogle(messageObj,recipientAdresses,res) {

     console.log(recipientAdresses);

    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
          user: process.env.EMAIL_SENDER,
          pass: process.env.EMAIL_PW
      }
  });
  var mailOptions = {
      from: process.env.EMAIL_SENDER,
      to: '' + recipientAdresses,
      subject: messageObj.subject,
      text: messageObj.body
  };
  transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
          return res.status(500).json({
              ok: false,
              status: 500,
              message: messageObj.error,
              data: {}
          });
      } else {


        res.status(200).json({
          ok: true,
          status:200,
          message:messageObj.success,
          data:messageObj.data
      });

      }
  });

  }


  async function sendWhatsAppSMS(jwt, messageObj, mobile,res) {

    var templateData = {
          data1: messageObj.code
    }

    let mobileNumber = mobile;
    if(mobile.charAt(0) === '0'){
      mobileNumber = mobile.substr(1);
      mobileNumber =  "62"+ mobileNumber;
    }

    var options = {
      method: 'POST',
      url: process.env.CLIENT_WHATSAPP_API,
      headers:
      {
        authorization: 'Bearer ' + jwt,
        'content-type': 'application/json'
      },
      body:
      {
        ticketID: '',
        source: 'OTP',
        templateID: 'otp',
        phone: mobileNumber,
        templateData:templateData,
        smsText: messageObj.body
      },
      json: true,
      strictSSL: false
    };

    let resp = await request(options).catch(err => {
      console.log("error");
      res.status(500).json({
        ok: false,
        status: 500,
        message:messageObj.error,
        data: {}
    });

    });

    if (!util.isNullOrUndefined(resp)) {

          if(resp.statusCode == 0){

            res.status(200).json({
              ok: true,
              status:200,
              message: "OTP Sent",
              data:messageObj.data
          });

          }
          else{

            if(resp.statusCode == 1){

              res.status(200).json({
                ok: false,
                status: 401,
                message: "Error sending SMS",
                data: {}
            });
            }else{
              res.status(500).json({
                ok: false,
                status: 500,
                message: "Error sending SMS",
                data: {}
            });
            }
          }
    }
    else {
      console.log("error");
      res.status(500).json({
        ok: false,
        status: 500,
        message: "Error sending WhatApp",
        data: {}
    });

    }

  }


  async function sendSMS(jwt, messageObj, mobile,res) {

    var options = {
      method: 'POST',
      url: process.env.CLIENT_SMS_API,
      headers:
      {
        authorization: 'Bearer ' + jwt,
        'content-type': 'application/json'
      },
      body:
      {
        senderid: 'INDIHOME',
        username: 'test',
        password: 'test',
        messageType: 'NOTIFY',
        msisdn: [mobile],
        message: messageObj.body
      },
      json: true,
      strictSSL: false
    };

    let resp = await request(options).catch(err => {
      console.log(err);
      res.status(500).json({
        ok: false,
        status: 500,
        message:"Error sending SMS",
        data: {}
    });

    });
    if (!util.isNullOrUndefined(resp)) {

          if(resp.data.responses.response.code == 1){

            res.status(200).json({
              ok: true,
              status:200,
              message: messageObj.success,
              data:messageObj.data
          });

          }
          else{

            if(resp.data.responses.response.code == 0){

              res.status(200).json({
                ok: false,
                status: 401,
                message: "Invalid mobile number",
                data: {}
            });
            }else{
              res.status(500).json({
                ok: false,
                status: 500,
                message: "Error sending SMS",
                data: {}
            });
            }
          }
    }
    else {
      console.log("error");
      res.status(500).json({
        ok: false,
        status: 500,
        message: "Server Error",
        data: {}
    });

    }

  }


  module.exports = {
    sendSMS,sendWhatsAppSMS,sendEmail,getJWT
  };
