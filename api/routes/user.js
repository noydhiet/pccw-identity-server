const express = require("express");
const router = express.Router();

const UserController = require('../controllers/user');
const checkAuth = require('../middleware/check-auth');
const checkBasicAuth = require('../middleware/basic_auth');

router.post("/signup",checkBasicAuth, UserController.user_signup);

router.get("/userCheck",checkBasicAuth, UserController.user_check);

router.post("/login",checkBasicAuth, UserController.user_login);

router.post("/guestLogin",checkBasicAuth, UserController.guest_login);

router.post("/forget",checkBasicAuth, UserController.forget_password);

router.post("/reset",checkBasicAuth, UserController.reset_password);

router.post("/verifyPasswordRecovery", checkBasicAuth, UserController.verify_recovery_code);

router.post("/newPassword",checkAuth,UserController.new_password);

router.post("/token",checkBasicAuth, UserController.refresh_token);

router.post("/active",checkBasicAuth, UserController.user_activation);

router.post("/block",checkBasicAuth, UserController.user_blocking);

router.delete("/:email", checkBasicAuth, UserController.user_delete);

router.get("/:userId",checkAuth, UserController.user_details);

router.post("/requestChangeEmail",checkAuth, UserController.reset_user_email);

router.post("/changeEmail", checkBasicAuth, UserController.change_user_email);

router.post("/requestChangeMobile",checkAuth, UserController.reset_user_mobile);

router.post("/changeMobile", checkBasicAuth, UserController.change_user_mobile);

router.post("/changeName",checkAuth, UserController.change_user_name);

router.post("/userInfomation",checkBasicAuth, UserController.user_save);

module.exports = router;
