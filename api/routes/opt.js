const express = require("express");
const router = express.Router();
const OtpController = require('../controllers/opt');
const checkBasicAuth = require('../middleware/basic_auth');


router.post("/:token",checkBasicAuth, OtpController.create_otp);

router.post("/:token/:code/:type",checkBasicAuth, OtpController.verify_otp);

module.exports = router;
